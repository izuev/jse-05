package ru.tsc.izuev.tm;

import ru.tsc.izuev.tm.constant.ArgumentConst;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    private static void showError(){
        System.out.println("[ERROR]");
        System.out.println("Current program arguments are not correct...");
    }

    public static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) {
            showError();
            return;
        }
        processArgument(arguments[0]);
    }

    private static void processArgument(final String argument){
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showError();
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
        System.exit(0);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Igor Zuev");
        System.out.println("E-mail: izuev@t1-consulting.ru");
        System.exit(0);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show version info.\n",ArgumentConst.VERSION);
        System.out.printf("%s - Show developer info.\n",ArgumentConst.ABOUT);
        System.out.printf("%s - Show command list.\n",ArgumentConst.HELP);
        System.exit(0);
    }

}